<!DOCTYPE html>
<html>
<head>
	<title>Cetak PDF</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
	<div class="container">
        <div>
            <h1 class="text-center">PDF Transaksi Pembelian</h1>
        </div>
		<table class="table table-borderless text-gray-800" width="100%" cellspacing="0">
            <tbody>
                <tr>
                    <td colspan="4">ID Pembelian</td>
                    <td>:</td>
                    <td>{{$tp->id}}</td>
                </tr>
                <tr>
                    <td colspan="4">Hari</td>
                    <td>:</td>
                    <td>{{$tp->hari_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Tanggal</td>
                    <td>:</td>
                    <td>{{$tp->tanggal_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Jam</td>
                    <td>:</td>
                    <td>{{$tp->jam_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Total Harga</td>
                    <td>:</td>
                    <td>{{$tp->total_harga}}</td>
                </tr>
            </tbody>
        </table>
        <div><br></div>
        <table class="table table-stripped" id="dtprod" width="100%" >
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Nama Barang</th>
                <th>Harga Jual</th>
                <th>QTY</th>
                <th>Subtotal</th>
            </tr>
            </thead>
            @php
                $totalakhir = 0;
            @endphp
            <tbody>
                @foreach ($tpb as $item)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                    <td>{{ $item->master_barang_id }}</td>
                    <td>{{ $item->master_barang->nama_barang }}</td>
                    <td>{{ $item->harga_satuan}}</td>
                    <td>{{ $item->jumlah }}</td>
                    <td>{{ $item->jumlah*$item->harga_satuan }}</td>
                </tr>
                @php
                    $totalakhir += $item->jumlah*$item->harga_satuan;
                @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td><strong>Total</strong></td>
                    <td>:</td>
                    <td>{{$totalakhir}}</td>
                </tr>
            </tfoot>
        </table>
	</div>
</body>
</html>

