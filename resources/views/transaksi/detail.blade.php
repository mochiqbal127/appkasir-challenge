@extends('master')
@section('title','Detail')
@section('content')

<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Detail Pembelian</h6>
        <div class="d-inline-block">
            <a href="{{ url('transaksi') }}" class="btn btn-danger shadow-sm"><i class="fa fa-chevron-circle-left"></i> Back</a>
            {{-- <button href="#add" data-toggle="modal" data-target="#cetakModal" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-print"></i> Export</button>
            <small class="text-gray-300">{{$p->kode_masuk}}</small> --}}
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table text-gray-800" width="100%" cellspacing="0">
            <thead>
                <tr>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4">ID Pembelian</td>
                    <td>:</td>
                    <td>{{$tp->id}}</td>
                </tr>
                <tr>
                    <td colspan="4">Hari</td>
                    <td>:</td>
                    <td>{{$tp->hari_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Tanggal</td>
                    <td>:</td>
                    <td>{{$tp->tanggal_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Jam</td>
                    <td>:</td>
                    <td>{{$tp->jam_display}}</td>
                </tr>
                <tr>
                    <td colspan="4">Total Harga</td>
                    <td>:</td>
                    <td>{{$tp->total_harga}}</td>
                </tr>
            </tbody>
        </table>

        </div>
    </div>
</div>
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Barang</h6>
        <div class="d-inline-block">

        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
        <table class="table table-hover" id="dtprod" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Nama Barang</th>
                <th>Harga Jual</th>
                <th>QTY</th>
                <th>Subtotal</th>
            </tr>
            </thead>
            @php
                $totalakhir = 0;
            @endphp
            <tbody>
                @foreach ($tpb as $item)
                <tr>
                    <td>{{ !empty($i) ? ++$i : $i = 1 }}</th>
                    <td>{{ $item->master_barang_id }}</td>
                    <td>{{ $item->master_barang->nama_barang }}</td>
                    <td>{{ $item->harga_satuan}}</td>
                    <td>{{ $item->jumlah }}</td>
                    <td>{{ $item->jumlah*$item->harga_satuan }}</td>
                </tr>
                @php
                    $totalakhir += $item->jumlah*$item->harga_satuan;
                @endphp
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td><strong>Total</strong></td>
                    <td>:</td>
                    <td>{{$totalakhir}}</td>
                </tr>
            </tfoot>
        </table>
        </div>
    </div>
</div>
<div class="d-sm-flex align-items-center justify-content-between">
    <div></div>
    <div class="d-inline-block">
        <a href="{{url("faktur/$tp->id")}}" class="btn btn-primary"><i class="fa fa-print"></i> Cetak PDF</a>
    </div>
</div>
<div>
    <br>
</div>

@endsection
{{-- @include('barang.form') --}}
@push('script')

@endpush
