@extends('master')

@section('title')
    Transaksi
@endsection

@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Transaksi</h6>
        <div class="d-inline-block">

        </div>
    </div>
    <div class="card-body" id="div-pilih">
        <div class="form-group">
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-6">
                    <select name="pilih_barang" id="pilih_barang" class="form-control">
                        <option value="" disabled selected>--Pilih Barang--</option>
                        @foreach ($barang as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label text-right">ID</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control bg-white" id="id_terpilih" >
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label text-right">Nama Barang</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control bg-white" id="barang_terpilih" >
            </div>
        </div>
        <div class="form-group row">
            <label for="staticEmail" class="col-sm-2 col-form-label text-right">Harga</label>
            <div class="col-sm-5">
              <input type="text" readonly class="form-control bg-white" id="harga_terpilih" >
            </div>
            <div class="col-sm-5">
                <button class="btn btn-success" id="btn-pilih"> Pilih</button>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('transaksi') }}" method="post" id="forminput">
            @csrf
            <div class="table-responsive">
                <table class="table table-hover" style="overflow: auto;" id="tblbarang">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Nama</td>
                            <td>Harga</td>
                            <td>Jumlah</td>
                            <td>Subtotal</td>
                            <td>Aksi</td>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td>Total :</td>
                            <td>Rp. <span id="totalHarga">0</span></td>
                            <td><input type="hidden" id="totalHargaInput" name="total_harga"></td>
                        </tr>
                    </tfoot>
                </table>

            </div>
            <div class="text-right">
                @if(auth()->user()->isAdmin == 1)
                    @if (count(App\Models\TransaksiPembelian::all())!=0)
                    <button type="button" id="btn-print" class="btn btn-secondary"><i class="fa fa-print"></i> Print Struk Transaksi Terakhir</button>
                    @endif
                @else
                    @if (count(App\Models\TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%')->get())!=0)
                    <button type="button" id="btn-print" class="btn btn-secondary"><i class="fa fa-print"></i> Print Struk Transaksi Terakhir</button>
                    @endif
                @endif
                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
            </div>
        </form>
    </div>
</div>
@if(auth()->user()->isAdmin == 1)
    @if (count(App\Models\TransaksiPembelian::all())!=0)
    <div class="d-none d-print-block" id="divprint">
        <div class="card">
            <div class="card-header">
                <strong>Struk Pembelian</strong>
            </div>
            <div class="card-body">
            <div class="mb-4">
                <div>
                <table>
                    <tbody>
                        <tr>
                            <td colspan="4">ID Pembelian</td>
                            <td>:</td>
                            <td>{{$tp->id}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Hari</td>
                            <td>:</td>
                            <td>{{$tp->hari_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Tanggal</td>
                            <td>:</td>
                            <td>{{$tp->tanggal_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Jam</td>
                            <td>:</td>
                            <td>{{$tp->jam_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Total Harga</td>
                            <td>:</td>
                            <td>{{$tp->total_harga}}</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
            <br>
            <div class="table-responsive-sm">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Barang</th>
                        <th>Harga Jual</th>
                        <th>QTY</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    @php
                        $totalakhir = 0;
                    @endphp
                    <tbody>
                        @foreach ($tpb as $item)
                        <tr>
                            <td>{{ $item->master_barang_id }}</td>
                            <td>{{ $item->master_barang->nama_barang }}</td>
                            <td>{{ $item->harga_satuan}}</td>
                            <td>{{ $item->jumlah }}</td>
                            <td>{{ $item->jumlah*$item->harga_satuan }}</td>
                        </tr>
                        @php
                            $totalakhir += $item->jumlah*$item->harga_satuan;
                        @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td><strong>Total</strong></td>
                            <td>:</td>
                            <td>{{$totalakhir}}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-5">

                </div>
            </div>

            </div>
        </div>
    </div>
    @endif
@else
    @if (count(App\Models\TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%')->get())!=0)
    <div class="d-none d-print-block" id="divprint">
        <div class="card">
            <div class="card-header">
                <strong>Struk Pembelian</strong>
            </div>
            <div class="card-body">
            <div class="mb-4">
                <div>
                <table>
                    <tbody>
                        <tr>
                            <td colspan="4">ID Pembelian</td>
                            <td>:</td>
                            <td>{{$tp->id}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Hari</td>
                            <td>:</td>
                            <td>{{$tp->hari_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Tanggal</td>
                            <td>:</td>
                            <td>{{$tp->tanggal_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Jam</td>
                            <td>:</td>
                            <td>{{$tp->jam_display}}</td>
                        </tr>
                        <tr>
                            <td colspan="4">Total Harga</td>
                            <td>:</td>
                            <td>{{$tp->total_harga}}</td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
            <br>
            <div class="table-responsive-sm">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama Barang</th>
                        <th>Harga Jual</th>
                        <th>QTY</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    @php
                        $totalakhir = 0;
                    @endphp
                    <tbody>
                        @foreach ($tpb as $item)
                        <tr>
                            <td>{{ $item->master_barang_id }}</td>
                            <td>{{ $item->master_barang->nama_barang }}</td>
                            <td>{{ $item->harga_satuan}}</td>
                            <td>{{ $item->jumlah }}</td>
                            <td>{{ $item->jumlah*$item->harga_satuan }}</td>
                        </tr>
                        @php
                            $totalakhir += $item->jumlah*$item->harga_satuan;
                        @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td><strong>Total</strong></td>
                            <td>:</td>
                            <td>{{$totalakhir}}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-5">

                </div>
            </div>

            </div>
        </div>
    </div>
    @endif
@endif
@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        var totalHarga = 0 ;
        $("#pilih_barang").select2();

        $('#pilih_barang').change(function(){
            var id = $(this).val();

            $.ajax({
                url: "{{ url('transaksibarang') }}/" + id,
                method: 'GET',
                success: function(data) {
                    console.log(id)
                    console.log(data)
                    $.each(data,function(index,merkObj){
                        $('#id_terpilih').val(merkObj.id)
                        $('#barang_terpilih').val(merkObj.nama_barang)
                        $('#harga_terpilih').val(merkObj.harga_satuan)
                    });
                }
            });
        });

        $('#forminput').on("click","#btn-print",function(){
            var prtContent = document.getElementById("divprint");
            var WinPrint = window.open('', '', 'left=0,top=0,width=1000,height=1000,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        });

        $('#div-pilih').on("click","#btn-pilih",function(){
            let id = $("#id_terpilih").val();
            let nama = $("#barang_terpilih").val();
            let harga = $("#harga_terpilih").val();
            if (id == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Pilih barang terlebih dahulu',
                    })
            }
            else {

            let idbarang = document.getElementsByClassName('id-barang');
            for (let i = 0; i < idbarang.length ; i++){
                if(idbarang[i].innerText == id){
                    Swal.fire({
                    icon: 'error',
                    title: 'Barang sudah dipilih...',
                    text: 'Silahkan ubah jumlah yang akan dibeli!',
                    })
                    return;
                }
            }

            let act = "<input type='button' value='x' class='btn btn-lg btn-danger conf-modal' data-toggle='modal' data-target='#confModal'>";

            let addRow = "<tr><td class='id-barang'>"+id+"<input type='hidden' value='"+id+"' readonly class='form-control bg-white' name='master_barang_id[]'></td>";
            addRow += "<td>"+nama+"</td>";
            addRow += "<td>"+harga+"<input type='hidden' value='"+harga+"' readonly class='form-control bg-white harga' name='harga_satuan[]'></td>";
            addRow += "<td><input type='number' value='1' min='1' class='form-control jumlah' name='jumlah[]'></td>";
            addRow += "<td class='subTotal'>"+harga+"<input type='hidden' value='"+harga+"' readonly class='form-control total bg-white' name='total[]'></td>";
            addRow += "<td>"+act+"</td></tr>";

            $('#tblbarang tbody').append(addRow);
            totalHarga += parseFloat(harga);
            $('#totalHarga').text(totalHarga);
            $('#totalHargaInput').val(totalHarga);
            }

        });
        $('#tblbarang').on('change','.jumlah',function(){
                let qty = parseInt($(this).closest('tr').find('.jumlah').val());
                let hargaBarang = parseInt($(this).closest('tr').find('td:eq(2)').text());
                let subAwal = parseInt($(this).closest('tr').find('.subTotal').text());
                let subTotal = qty * hargaBarang;
                totalHarga += subTotal - subAwal;
                $(this).closest('tr').find('.subTotal').text(subTotal);
                $('#totalHarga').text(totalHarga);
                $('#totalHargaInput').val(totalHarga);
        });
        $('#tblbarang').on('keyup', '.jumlah', function () {
                if (isNaN($(this).val()) || $(this).val() < 1) {
                    $(this).val('1');
                }
        });
        $('#tblbarang').on('click','.conf-modal',function(){
                let subAwal = parseFloat($(this).closest('tr').find('.subTotal').text());
                totalHarga-=subAwal;

                crow = $(this).closest('tr').remove();
                $('#totalHarga').text(totalHarga);
                $('#totalHargaInput').val(totalHarga);
            });
    });

</script>
@endpush
