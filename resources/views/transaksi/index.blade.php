@extends('master')

@section('title')
    History Transaksi
@endsection

@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        @if (auth()->user()->isAdmin == 0)
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Transaksi Hari Ini</h6>
        @else

        <h6 class="h5 m-0 font-weight-bold text-gray-100">History Transaksi</h6>
        @endif
        <div class="d-inline-block">
            <a href="{{ url('transaksi') }}" class="btn btn-sm btn-secondary shadow-sm"><i class="fa fa-redo"></i> Refresh</a>
            <a href="{{ url('transaksi/input') }}" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah</a>
            {{-- <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahData">
                <i class="fa fa-plus"></i> Tambah
            </button> --}}
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ url('/transaksi/search') }}">
            @csrf
            <div class="row">
                <div class="row col">
                    <input type="text" class="form-control col-sm-2 bg-white border-0" value="Rentang Harga" readonly>
                    <input type="number" class="form-control col" required name="harga_min">
                    <input type="text" class="form-control col-sm-2 text-center bg-white border-0" value="sampai" readonly>
                    <input type="number" class="form-control col" required name="harga_max">
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
          </form>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover"  width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>ID</td>
                        <td>Waktu</td>
                        <td style="display: none">Barang</td>
                        <td>Total Harga</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($history as $tp)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{ $tp->id }}</td>
                        <td>{{ $tp->date_display }}</td>
                        <td style="display: none">
                            @foreach ($tp->transaksi_pembelian_barang as $item)
                                {{ $item->master_barang->nama_barang.',' }}
                            @endforeach
                        </td>
                        <td class="text-right">Rp. {{number_format($tp->total_harga, 0 , ',', '.')}}</td>
                        <td>
                            <a href="{{ url('transaksi').'/'.$tp->id}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                            <a href="{{ url('faktur').'/'.$tp->id}}" class="btn btn-danger"><i class="fas fa-print"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $(".table").DataTable();
    });

</script>
@endpush
