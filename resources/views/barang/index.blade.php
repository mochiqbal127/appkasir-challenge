@extends('master')

@section('title')
    Barang
@endsection

@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List Barang</h6>
        <div class="d-inline-block">
            {{-- <a href="{{ url('dashboard/item/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a> --}}
            <a href="{{ url('/barang') }}" class="btn btn-sm btn-secondary"><i class="fa fa-redo"></i> Refresh</a>
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#tambahData">
                <i class="fa fa-plus"></i> Tambah
            </button>
        </div>
    </div>
    <div class="modal fade" id="tambahData" tabindex="-1" aria-labelledby="tambahDataLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahDataLabel">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('/barang') }}" method="post">
                @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama Barang</label>
                            <input type="text" name="nama_barang" id="nama" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="nama">Harga Satuan</label>
                            <input type="number" name="harga_satuan" id="harga" class="form-control" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ url('/barang/search') }}">
            @csrf
            <div class="row">
                <div class="row col">
                    <input class="form-control bg-white col-sm-2 border-0" value="Nama" readonly>
                    <input type="text" class="form-control col" name="nama_barang">
                </div>
                <div class="row col">
                    <input class="form-control bg-white col-sm-3 border-0 text-right" readonly value="Harga">
                    <select name="operator" class="form-control col-sm-1" id="">
                        <option value=">">></option>
                        <option value="<"><</option>
                        <option value="=">=</option>
                    </select>
                    <input type="number" class="form-control col" name="harga_satuan">
                </div>
                <div class="col-sm-1">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
          </form>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover"  width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Harga Satuan</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($barang as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$item->nama_barang}}</td>
                        <td>{{$item->harga_satuan}}</td>
                        <td>
                            <form action="barang/{{$item->id}}" method="post">
                                {{-- <a href="barang/{{$item->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a> --}}
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editData{{$item->id}}">
                                    <i class='fa fa-edit px-1'></i>
                                </button>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    <div class="modal fade" id="editData{{$item->id}}" tabindex="-1" aria-labelledby="editDataLabel{{$item->id}}" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="editDataLabel{{$item->id}}">Edit Barang</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form action="/barang/{{$item->id}}" method="post">
                              @csrf
                              @method('put')
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="nama">Nama Barang</label>
                                        <input type="text" name="nama_barang" id="nama" class="form-control" required value="{{ $item->nama_barang }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Harga Satuan</label>
                                        <input type="number" name="harga_satuan" id="harga" class="form-control" required value="{{ $item->harga_satuan }}">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </form>
                          </div>
                        </div>
                    </div>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $(".table").DataTable({searching: false, lengthChange : false});
    });

</script>
@endpush
