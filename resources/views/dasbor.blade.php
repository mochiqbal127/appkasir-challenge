@extends('master')
@section('title')
  Dashboard
@endsection
@section('content')
	 <div class="container-fluid py-4">
      <div class="row">
        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-capitalize font-weight-bold">Total Barang</p>
                    <h5 class="font-weight-bolder mb-0">
                      {{count(App\Models\MasterBarang::all())}}
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="ni ni-world text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
          <div class="card">
            <div class="card-body p-3">
              <div class="row">
                <div class="col-8">
                  <div class="numbers">
                    <p class="text-sm mb-0 text-capitalize font-weight-bold">Total Order</p>
                    <h5 class="font-weight-bolder mb-0">
                      {{count(App\Models\TransaksiPembelian::all())}}
                    </h5>
                  </div>
                </div>
                <div class="col-4 text-end">
                  <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                    <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-sm-6 mb-xl-0 mb-4">
            <div class="card">
              <div class="card-body p-3">
                <div class="row">
                  <div class="col-8">
                    <div class="numbers">
                        @if (auth()->user()->isAdmin == 0)
                        <p class="text-sm mb-0 text-capitalize font-weight-bold">Pendapatan Hari Ini</p>
                        <h5 class="font-weight-bolder mb-0">
                          Rp. {{$uangsekarang}}
                        </h5>
                        @else

                        <p class="text-sm mb-0 text-capitalize font-weight-bold">Total Pendapatan</p>
                        <h5 class="font-weight-bolder mb-0">
                          Rp. {{$uang}}
                        </h5>
                        @endif
                    </div>
                  </div>
                  <div class="col-4 text-end">
                    <div class="icon icon-shape bg-gradient-primary shadow text-center border-radius-md">
                      <i class="ni ni-paper-diploma text-lg opacity-10" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
@endsection
@if(session('error'))

  @push('scripts')
  <script>
    {!! session('error') !!}

  </script>

  @endpush
@endif
