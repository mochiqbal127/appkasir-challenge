@extends('master')
@section('title')
	Tambah User
@endsection
@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Tambah User</h6>
        <div class="d-inline-block">
            <a href="{{ url('user') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-chevron-circle-left"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="form">
            <form action="{{ url('user') }}" method="post" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Username :</label>
					<input type="text" name="name" id="name" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="password">Password :</label>
					<input type="password" name="password" id="password" class="form-control" required>
				</div>
				<div class="form-group">
					<label for="isAdmin">Role</label>
					<select name="isAdmin" id="isAdmin" class="form-control" required>
						<option disabled selected>-- Pilih Role --</option>
						<option value="1">Admin</option>
						<option value="0">Kasir</option>
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>Simpan</button>
				</div>
			</form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
    $('.select2').select2();
});
</script>
@endpush

@push('style')

@endpush
