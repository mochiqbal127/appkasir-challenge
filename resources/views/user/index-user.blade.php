@extends('master')
@section('title')
	User
@endsection
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">List User</h6>
        <div class="d-inline-block">
            <a href="{{ url('user/create') }}" class="btn btn-sm btn-success shadow-sm"><i class="fa fa-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover" style="overflow: auto;">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Email</td>
                        <td>Role</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $item)
                    <tr>
                        <td>{{ !empty($i) ? ++$i : $i = 1 }}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->role_display}}</td>
                        <td>
                            <form action="user/{{$item->id}}" method="post">
                                <a href="user/{{$item->id}}/edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>

        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        $(".table").DataTable();
    });
</script>
@endpush
@if(session('success'))

  @push('scripts')
  <script>
    {!! session('success') !!}

  </script>

  @endpush
@endif
