@extends('master')
@section('title')
	Edit User
@endsection
@section('content')
@include('part.feedback')
<div class="card shadow mb-4">
    <div class="card-header py-3 bg-primary d-sm-flex align-items-center justify-content-between">
        <h6 class="h5 m-0 font-weight-bold text-gray-100">Edit User</h6>
        <div class="d-inline-block">
            <a href="{{ url('user') }}" class="btn btn-sm btn-danger shadow-sm"><i class="fa fa-chevron-circle-left"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <div class="form">
            <form action="{{ url('user').'/'.$user->id }}" method="post" enctype="multipart/form-data">
				@csrf
                @method('put')
				<div class="form-group">
					<label for="name">Username :</label>
					<input type="text" name="name" id="name" class="form-control" required value="{{ $user->name }}">
				</div>
				<div class="form-group">
					<label for="email">Email :</label>
					<input type="text" name="email" id="email" class="form-control" required value="{{ $user->email }}">
				</div>
				<div class="form-group">
					<label for="password">New Password :</label>
					<input type="password" name="password" id="password" class="form-control">
				</div>
				<div class="form-group">
					<label for="isAdmin">Role</label>
					<select name="isAdmin" id="isAdmin" class="form-control" required>
						<option>-- Pilih Role --</option>
						<option value="1" {{ @$user->isAdmin == 1 ? 'selected' : ''}}>Admin</option>
						<option value="0" {{ @$user->isAdmin == 0 ? 'selected' : ''}}>Kasir</option>
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i>Simpan</button>
				</div>
			</form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
    $('.select2').select2();
});
</script>
@endpush

@push('style')

@endpush
