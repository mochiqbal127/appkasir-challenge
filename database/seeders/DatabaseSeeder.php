<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\MasterBarang;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        MasterBarang::create([
            'nama_barang' => 'Sabun batang',
            'harga_satuan' => '3000',
            'isActive' => '1',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Mi instan',
            'harga_satuan' => '2000',
            'isActive' => '1',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Pensil',
            'harga_satuan' => '1000',
            'isActive' => '1',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Kopi sachet',
            'harga_satuan' => '1500',
            'isActive' => '1',
        ]);
        MasterBarang::create([
            'nama_barang' => 'Air minum galon',
            'harga_satuan' => '20000',
            'isActive' => '1',
        ]);
    }
}
