<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\MasterBarang;
use \App\Models\TransaksiPembelian;
use Illuminate\Support\Facades\DB;
use \App\Models\TransaksiPembelianBarang;
use PDF;

class TransaksiController extends Controller
{
    public function index(){
        $barang = MasterBarang::where('isActive',1)->get();
        if (auth()->user()->isAdmin == 0) {
            $tp = TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%')->latest()->first();
        } else {
            $tp = TransaksiPembelian::latest()->first();
        }

        // $tp = TransaksiPembelian::latest()->first();

        if ($tp == null) {
            return view('transaksi.input',compact('barang'));
        }
        else{
            $tpb = TransaksiPembelianBarang::with('master_barang')->where('transaksi_pembelian_id',$tp->id)->get();
            return view('transaksi.input',compact('barang','tp','tpb'));
        }

    }
    public function getbarang(Request $request,$id){
        $barang = MasterBarang::where('id',$id)->get();
        return response()->json($barang);
    }
    public function store(Request $request){
        $rules= [
            'total_harga'      => 'required',

        ];
        $messages = [
            'total_harga.required'        => 'Tidak ada barang yang dibeli',
        ];
        $this->validate($request,$rules,$messages);

        if ($request['jumlah']) {

            $data = new TransaksiPembelian();
            $data->total_harga = $request->total_harga;
            $data->save();

            $last = TransaksiPembelian::select('id')->orderBy('created_at','desc')->first();
            $lastId = $last->id;
            for($i=0 ; $i < count($request['jumlah']) ;$i++){
                $input = new TransaksiPembelianBarang();
                $input->transaksi_pembelian_id = $lastId;
                $input->master_barang_id = $request->master_barang_id[$i];
                $input->harga_satuan = $request->harga_satuan[$i];
                $input->jumlah = $request->jumlah[$i];
                $status=$input->save();
            }
            if($status){
                return redirect('/transaksi/input')->with('success','Data Berhasil Disimpan!');
            }else{
                return redirect('/transaksi/input')->with('error','Data Gagal Disimpan!');
            }
        }else{
            return redirect('/transaksi/input')->with('error','Data Gagal Disimpan!');
        }

    }
    public function show_all_pembelian()
    {   if (auth()->user()->isAdmin == 1) {
            $history = TransaksiPembelian::all()->sortByDesc('created_at');
        }else{
            $history = TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%')->get()->sortByDesc('created_at');
        }

        return view('transaksi.index',compact('history'));
    }
    public function show_detail_pembelian($id)
    {
        $tp = TransaksiPembelian::find($id);
        $tpb = TransaksiPembelianBarang::with('master_barang')->where('transaksi_pembelian_id',$id)->get();
        return view('transaksi.detail',compact('tp','tpb'));
    }
    public function search(Request $request)
    {
        $harga_min = $request->harga_min;
        $harga_max = $request->harga_max;

        if (auth()->user()->isAdmin == 1) {
            if ($harga_min < $harga_max) {
                $history = TransaksiPembelian::where('total_harga','>=',$harga_min,'and')->where('total_harga','<=',$harga_max)->get()->sortBy('total_harga');
            }else if ($harga_min > $harga_max) {
                $history = TransaksiPembelian::where('total_harga','<=',$harga_min,'and')->where('total_harga','>=',$harga_max)->get()->sortByDesc('total_harga');
            }else{
                $history = TransaksiPembelian::where('total_harga','=',$harga_min,'and')->where('total_harga','=',$harga_max)->get()->sortByDesc('created_at');
            }
        }else{
            if ($harga_min < $harga_max) {
                $history = TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%','and')->where('total_harga','>=',$harga_min,'and')->where('total_harga','<=',$harga_max)->get()->sortBy('total_harga');
            }else if ($harga_min > $harga_max) {
                $history = TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%','and')->where('total_harga','<=',$harga_min,'and')->where('total_harga','>=',$harga_max)->get()->sortByDesc('total_harga');
            }else{
                $history = TransaksiPembelian::where('created_at','like','%'.date("Y-m-d").'%','and')->where('total_harga','=',$harga_min,'and')->where('total_harga','=',$harga_max)->get()->sortByDesc('created_at');
            }
        }


        // $history = TransaksiPembelian::where('total_harga','>=',$harga_min,'and')->where('total_harga','<=',$harga_max)->get();

        return view('transaksi.index',compact('history'));
    }
    public function cetak_pdf($id){
        set_time_limit(240);
        $tp = TransaksiPembelian::find($id);
        $tpb = TransaksiPembelianBarang::with('master_barang')->where('transaksi_pembelian_id',$id)->get();
        // return view('transaksi.pdf',compact('tp','tpb'));

        $pdf   = PDF::loadview('transaksi.pdf',compact('tp','tpb'));
        return $pdf->stream('cetak-pdf-transaksi-'.$id.'.pdf');
    }
}
