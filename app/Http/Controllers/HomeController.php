<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $uang = 0;
        foreach (\App\Models\TransaksiPembelian::all() as $value) {
            $uang+=$value->total_harga;
        }
        $uangsekarang = 0;
        foreach (\App\Models\TransaksiPembelian::where('created_at','like',date('Y-m-d'.'%'))->get() as $value) {
            $uangsekarang += $value->total_harga;
        }

        return view('dasbor',compact('uang','uangsekarang'));
    }
}
