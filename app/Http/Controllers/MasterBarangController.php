<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use \App\Models\MasterBarang;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = MasterBarang::where('isActive',1)->get();
        return view('barang.index',compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'nama_barang' => 'required',
            'harga_satuan' => 'required',
        ];
        $this->validate($request, $rules);
        $status = MasterBarang::create([
            'nama_barang' => $request->nama_barang,
            'harga_satuan' => $request->harga_satuan,
            'isActive' => 1,
        ]);
        if($status) return redirect('/barang')->with('success','Data Berhasil Disimpan!!');
        else return redirect('/barang')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'nama_barang' => 'required',
            'harga_satuan' => 'required',
        ];
        $this->validate($request, $rules);
        $barang = MasterBarang::find($id);

        $status = $barang->update([
            'nama_barang'=>$request->nama_barang,
            'harga_satuan'=>$request->harga_satuan,
        ]);
        if($status) return redirect('/barang')->with('success',"Data berhasil DiUpdate");
        else return redirect('/barang')->with('error','Data gagal DiUpdate!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = MasterBarang::find($id);
        $barang->isActive = 0;
        $status = $barang->save();

        if($status) return redirect('/barang')->with('success',"Data berhasil diHapus");
        else return redirect('/barang')->with('error','Data gagal diHapus!!');

    }

    public function search(Request $request){
        $nama = $request->nama_barang;
        $harga = $request->harga_satuan;
        $operator = $request->operator;

        if ($harga == '') {
            $barang = DB::table('master_barang')
                    ->where('nama_barang', 'like', '%'.$nama.'%')
                    ->where('isActive',1)
                    ->get();
        }else{
            $barang = DB::table('master_barang')
                    ->where('nama_barang', 'like', '%'.$nama.'%')
                    ->where('harga_satuan', $operator, $harga)
                    ->where('isActive',1)
                    ->get();
        }

        return view('barang.index',compact('barang'));
    }
}
