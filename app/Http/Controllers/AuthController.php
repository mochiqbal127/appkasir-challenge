<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class AuthController extends Controller
{
    function loginView(){
        return view('auth.login');
    }
    function registerView() {
        return view('auth.register');
    }
    function register(Request $request){
        $rules= [
            'name'      => 'required',
            'email'     => 'required|email:dns|unique:users',
            'password'  => 'required',
        ];
        $messages = [
            'email.required'        => 'Email wajib diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'email.unique'       => 'Email sudah ada',
            'name.required'       => 'Username wajib diisi',
        ];
        $this->validate($request,$rules,$messages);

        $status = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'isAdmin' => '0',
        ]);
        if ($status) {
            return redirect('/signin');
        }else{
            return back()->with('error','Gagal Register');
        }
    }
    public function login(Request $request){
        $validatedData = $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);
        if(Auth::attempt($validatedData)){
            $request->session()->regenerate();
            return redirect()->intended('/');

        }
        return back()->with('error','Email atau Password anda salah!');

    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/signin');
    }
}
