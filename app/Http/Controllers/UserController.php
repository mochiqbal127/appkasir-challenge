<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use \App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();
        return view('user.index-user', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create-user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules= [
            'name'      => 'required',
            'email'     => 'required|email:dns|unique:users',
            'password'  => 'required',
            'isAdmin'   => 'required'
        ];
        $messages = [
            'email.required'        => 'Email wajib diisi',
            'email.email'           => 'Email tidak valid',
            'password.required'     => 'Password wajib diisi',
            'email.unique'       => 'Email sudah ada',
            'name.required'       => 'Username wajib diisi',
            'isAdmin.required'       => 'Wajib pilih role',
        ];
        $this->validate($request,$rules,$messages);

        $status = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'isAdmin' => $request->isAdmin,
        ]);

        if($status) return redirect('user')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Disimpan',
            'success'
          )"
        );

        else return redirect('user')->with('error','Data gagal Disimpan!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit-user', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->email != $request->email) {
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required',
                    'email'     => 'required|email:dns|unique:users',
                    'password'  => 'required',
                    'isAdmin'   => 'required',
                ]);

                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required',
                    'email'     => 'required|email:dns|unique:users',
                    'isAdmin'   => 'required',
                ]);
                $status = $user->update($validatedData);
            }
        }else{
            if ($request->password != '') {
                $validatedData = $request->validate([
                    'name'      => 'required',
                    'password'  => 'required',
                    'isAdmin'   => 'required',
                ]);
                $validatedData['password']  = Hash::make($validatedData['password']);

                $status = $user->update($validatedData);
            }else{
                $validatedData = $request->validate([
                    'name'      => 'required',
                    'isAdmin'   => 'required',
                ]);
                $status = $user->update($validatedData);
            }
        }


        if($status) return redirect('user')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil Diubah',
            'success'
          )"
        );

        else return redirect('user')->with('error','Data gagal Diubah!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $status = $user->delete();

        if($status) return redirect('user')->with('success',"Swal.fire(
            'Success!',
            'Data Berhasil dihapus',
            'success'
          )");
        else return redirect('user')->with('error','Data gagal dihapus!!');
    }
}
