<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\Carbon;
use Carbon\Carbon;


class TransaksiPembelian extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'transaksi_pembelian';

    public function transaksi_pembelian_barang(){
    	return $this->hasMany(TransaksiPembelianBarang::class);
    }
    public function getDateDisplayAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('l, d F Y H:i');
    }
    public function getHariDisplayAttribute(){
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('l');
    }
    public function getTanggalDisplayAttribute(){
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('d/m/Y');
    }
    public function getJamDisplayAttribute(){
        return Carbon::parse($this->attributes['created_at'])->translatedFormat('H:i');
    }
}
