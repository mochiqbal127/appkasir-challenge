<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiPembelianBarang;

class MasterBarang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'master_barang';

    public function transaksi_pembelian_barang(){
    	return $this->hasMany(TransaksiPembelianBarang::class);
    }
}
