<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterBarang;
use App\Models\TransaksiPembelian;


class TransaksiPembelianBarang extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = 'transaksi_pembelian_barang';

    public function master_barang(){
    	return $this->belongsTo(MasterBarang::class);
    }
    public function transaksi_pembelian(){
    	return $this->belongsTo(TransaksiPembelian::class);
    }
}
