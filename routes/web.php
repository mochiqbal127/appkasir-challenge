<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::group(['middleware' => ['guest']], function () {
	Route::get('/signin', 	[AuthController::class, 'loginView'])->name('login');
	Route::post('/signin', 	[AuthController::class, 'login']);

	Route::get('/signup', 	[AuthController::class, 'registerView']);
	Route::post('/signup',	[AuthController::class, 'register']);

});
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [HomeController::class, 'index']);

    Route::resource('/barang', MasterBarangController::class)->middleware('admin');
    Route::post('/barang/search', [MasterBarangController::class, 'search']);

    Route::resource('/user', UserController::class)->middleware('admin');

    Route::get('/transaksi/input', [TransaksiController::class, 'index']);
    Route::post('/transaksi', [TransaksiController::class, 'store']);
    Route::get('/transaksi', [TransaksiController::class, 'show_all_pembelian']);
    Route::post('/transaksi/search', [TransaksiController::class, 'search']);
    Route::get('/transaksi/{id}', [TransaksiController::class, 'show_detail_pembelian']);
    Route::get('/transaksibarang/{id}', [TransaksiController::class, 'getbarang'])->name('transaksibarang');

    Route::get('faktur/{id}', [TransaksiController::class, 'cetak_pdf']);

    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/logout',  [AuthController::class, 'logout']);

});
