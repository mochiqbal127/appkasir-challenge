# App Kasir

## Tentang

-   Framework : Laravel v8
-   SQL Database Server : MySQL (<i>recommend</i>)

## Instalasi

1.  Clone repositori gitlab
2.  Run command `composer install` atau `composer update`
3.  Buat file `.env` , copy seluruh isi file `.env.example` ke `.env`, lalu sesuaikan database beserta usernya.
3.  Run command `php artisan key:generate`
4.  Run command `php artisan migrate --seed`
5.  Untuk penggunaan, run command `php artisan serve`

## Demo Video Penggunaan dan Fitur

-   Link : [Gdrive](https://drive.google.com/file/d/1po0xIlJxtd3ONhVVdx1XKCXTMwhVz-GV/view?usp=sharing)
